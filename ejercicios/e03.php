<?php session_start(); ?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Ejercicio 03</title>
	<style>
		.debug{background-color:lightpink;}
	</style>
</head>

<body>
	<div id="enunciado" style="background-color: lightgray;">
	<h1>Enunciado</h1>
	<p>Crea un html con un formulario en el que aparezcan los siguientes campos:</p>
	<ul>
		<li>Nombre</li>
		<li>Apellido</li>
		<li>Edad</li>
		<li>Nota</li>
	</ul>
	<p>Al enviar este formulario al servidor, se deben cumplir los siguientes requerimientos:</p>
	<ol>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Hay cinco alumnos PRECARGADOS que han sacado diferentes calificaciones a lo largo del curso en diferentes tareas asignadas:
			<ul>
				<li>Eugenio Martínez de 45 años - Notas de tareas: 7, 6, 5, 8, 5, 6, 9, 10</li>
				<li>Marta Carrera de 22 - Notas de tareas: 1, 6, 2, 3, 5, 6, 9, 10, 10, 9</li>
				<li>Nacho Herrera de 25 - Notas de tareas: 3, 4, 2, 4, 6, 7, 9, 10, 3, 7</li>
				<li>Anxo Iglesias de 32 - Notas de tareas: 1, 6, 2, 3'2, 5, 2'2, 4'7, 5'5, 9, 9</li>
				<li>Valentina Iglesias de 30 - Notas de tareas: 9, 7</li>
			</ul>
		</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>La estructura de datos elegida es un array indexado por las claves nombre, apellido, edad, notas para cada alumno. Y un array que agrupe estos alumnos</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Es condición necesaria que toda esta estructura estea referenciada por una única variable para que pueda ser almacenada fácilmente en una variable de sesión</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Haz una lista que enumere los alumnos y sus notas medias.</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Calcula notas medias en función de los rangos de edad [18,29], [30,39], [más de 40]</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos</li>
		<li>Todos los datos son obligatorios y ninguno debe de enviarse vacío.</li>
		<li>La estructura de datos debe almacenarse en la sesión del usuario que realiza las peticiones.</li>
		<li>Si no hay ningún alumno coincidente en nombre y apellido, se añadirá este a la estructura de datos.</li>
		<li>Si hay un alumno coincidente en nombre y apellido, se añadirá a este alumno, la nueva nota enviada por formulario.</li>
		<li>Debe existir un botón en el formulario con el texto 'Matar Sesion' que termine con la sesión y consecuentemente con sus datos.</li>
	</ol>
	<p>Es necesario que esta sea entregada en plazo para la evaluación</p>
	</div>

	<h1>Resultado</h1>
<?php




			if(cerrarSesion()){

			session_destroy();

			mostrarBotonIniciar();

			return;
		}

		if(!datosExisten()){
			cargarDatos();
		}

		datosNuevos();

		mostrarForm();
		mostrarBotonCerrar();
mostrarAlumnos();
		mostrarAptos();

		mostrarMediadEdad();


		function mostrarForm(){
			echo '<div><form action="e03.php" method="GET">
	<input type="hidden" name="haz" value="registrar">
				Nombre:<input type="text" name="nombre"> <br>
				Apellido:<input type="text" name="apellido"> <br>
				Edad:<input type="text" name="edad"> <br>
				Nota:<input type="text" name="nota"> <br>

				<input type="submit" value="enviar">
			</form> </div>';
		}


		function mostrarBotonCerrar(){
			echo '<div><FORM ACTION="e03.php" method="GET">
				<input type="hidden" NAME="haz" value="matarsesion">
				<input type="submit" VALUE="cerrar sesion">
			</form></div> ';
		echo '<div style="clear:both;"></div>';
		}


		function mostrarBotonIniciar(){
			echo "Sesion destruída!";
		echo '<div><form action="e03.php" method="GET">
				<input type="submit" value="Reiniciar sesion">
			</form></div> ';
		}

			function calcularMediaEdad($data, $edadMinima, $edadMaxima){
					$sumar = 0;
				$contar= 0;

				foreach ( $data as $alu ) {
					if($alu["edad"] >= $edadMinima and $alu["edad"] <= $edadMaxima){
						$contar++;
						$sumar += calcularMedia($alu['notas']);
					}
				}
				return $sumar/$contar;
			}

			function calcularMedia($Notas) {
					$numeroNotas = 0;
			$sumarNotas = 0;

					foreach ( $Notas as $nota ) {
						$numeroNotas++;
						$sumarNotas += $nota;
			}
			return $sumarNotas/$numeroNotas;
			}



						function datosExisten(){
							return $_SESSION['hacer'] != NULL;
					}

			function cerrarSesion(){
				return $_GET['haz'] == "matarsesion";
			}




		function cargarDatos(){
			$Eugenio= [
			'nombre' => 'Eugenio',
			'apellido' => 'Martínez',
			'notas' => [7,6,5,8,5,6,9,10],
			'edad' => 45

			];

			$Marta= [
			'nombre' => 'Marta',
			'apellido' => 'Carrera',
			'notas' => [1,6,2,3,5,6,9,10,10,9],
			'edad' => 22

			];

			$Nacho= [
			'nombre' => 'Nacho',
			'apellido' => 'Herrera',
			'notas' => [3,4,2,4,6,7,9,10,3,7],
			'edad' => 25

			];

			$Anxo= [
			'nombre' => 'Anxo',
			'apellido' => 'Iglesias',
			'notas' => [1,6,2,3.2,5.0,2.2,4.7,5.5,9,9],
			'edad' => 32

			];

			$Valentina= [
			'nombre' => 'Valentina',
			'apellido' => 'Iglesias',
			'notas' => [9,7],
			'edad' => 30

			];

			$hacer=[
				$Eugenio,
				 	$Marta,
				 		$Nacho,
				 			$Anxo,
				  			$Valentina
				];
			$_SESSION['hacer'] = $hacer;

		}


		function mostrarMediadEdad(){

			$hacer = $_SESSION['hacer'];

				echo "<div>";

				echo "<h2>Notas:</h2>";
				echo "<br>Los alumnos con una edad entre [18 y 29] tienen una nota media de: " . calcularMediaEdad($hacer, 18, 29);
				echo "<br>Los alumnos con una edad entre [29 y 39] tienen una nota media de: " . calcularMediaEdad($hacer, 30, 39);
				echo "<br>Los alumnos con una edad de [40+] tienen una nota media de: " . calcularMediaEdad($hacer, 40, 100);

				echo "</div>";
		}


		function mostrarAptos(){

			 $hacer = $_SESSION['hacer'];

			 echo "<div>";
				 echo "<h2>Estos son los alumnos aptos </h2>";
				 echo "<ul>";
			 foreach ( $hacer as $alu ) {
				 if(count($alu['notas'])>=5 and calcularMedia($alu['notas'])>=5){
					 echo "<li>", $alu['nombre'] , " " ,$alu['apellido'] , ". Nota media: ", calcularMedia($alu['notas']) , "</li>";
				 }
				 }
			 echo "</ul>";
				 echo "</div>";
		 }


		function mostrarAlumnos(){
			echo "<div>";
				echo "<h2>Lista de Alumnos </h2>";
				echo "</div>";
			echo "<ul>";

			$hacer = $_SESSION['hacer'];

			foreach ( $hacer as $alu ) {

				echo "<li>". $alu['nombre'] . " " .$alu['apellido'] . ". Nota media: ". calcularMedia($alu['notas']) . "</li>";

				}

				echo "</ul>";
		}






		function getCodAlu($nombre, $ape, $alus){

			$i = 0;


			foreach($alus as $alum){


				if($alum['nombre']==$nombre and $alum['apellido']==$ape){
					return $i;
				}
				$i++;
			}

			return -10;
		}


		function datosNuevos(){
			if($_GET['haz'] == "registrar"){

				if($_GET['nombre'] != NULL && $_GET['apellido'] != NULL && $_GET['edad'] != NULL && $_GET['nota'] != NULL){
						if($_GET['nota'] <= 10){
					$codAlu = getCodAlu($_GET['nombre'],$_GET['apellido'], $_SESSION['hacer']);

					if($codAlu==-10){

						$alumnoX = [
							'nombre' => $_GET['nombre'],
							'apellido' => $_GET['apellido'],
							'notas' => [(int)$_GET['nota']],
							'edad' => (int)$_GET['edad']

						];

						$_SESSION['hacer'][count($_SESSION['hacer'])] = $alumnoX;
					}
					else {

						$_SESSION['hacer'][$codAlu]["notas"][count($_SESSION['hacer'][$codAlu]["notas"])] = (int)$_GET['nota'];
					}
				}else{
					echo'<script type="text/javascript">
    alert("Nota inválida");

    </script>';
				}
			}
			}

		}





?>

</body>

<html>
