<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Ejercicio 02</title>
</head>

<body>
	<div id="enunciado" style="background-color: lightgray;">
	<h1>Enunciado</h1>
	<p>En un script php debes almacenar en una estructura de datos la siguiente información:</p>
	<ol>
		<li>Hay cinco alumnos que han sacado diferentes calificaciones a lo largo del curso en diferentes tareas asignadas:
			<ul>
				<li>Eugenio Martínez de 45 años - Notas de tareas: 7, 6, 5, 8, 5, 6, 9, 10</li>
				<li>Marta Carrera de 22 - Notas de tareas: 1, 6, 2, 3, 5, 6, 9, 10, 10, 9</li>
				<li>Nacho Herrera de 25 - Notas de tareas: 3, 4, 2, 4, 6, 7, 9, 10, 3, 7</li>
				<li>Anxo Iglesias de 32 - Notas de tareas: 1, 6, 2, 3'2, 5, 2'2, 4'7, 5'5, 9, 9</li>
				<li>Valentina Iglesias de 30 - Notas de tareas: 9, 7</li>
			</ul>
		</li>
		<li>La estructura de datos elegida es un array indexado por las claves nombre, apellido, edad, notas para cada alumno. Y un array que agrupe estos alumnos</li>
		<li>Es condición necesaria que toda esta estructura estea referenciada por una única variable para que pueda ser almacenada fácilmente en una variable de sesión</li>
		<li>Haz una lista que enumere los alumnos y sus notas medias.</li>
		<li>Calcula notas medias en función de los rangos de edad [18,29], [30,39], [más de 40]</li>
		<li>Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos</li>
	</ol>

	<p>Es necesario que esta sea entregada en plazo para la evaluación</p>

	</div>

	<h1>Resultado</h1>
<?php
$alumno1 = [
	"nombre" => "Eugenio" ,
	"apellido" => "Martinez",
	"edad" => "45",
	"notas" => array(7,6,5,8,5,6,9,10),
];
$alumno2 = [
	"nombre" => "Marta" ,
	"apellido" => "Carrera",
	"edad" => "22",
	"notas" => array(1,6,2,3,5,6,9,10,10,9),
];
$alumno3 = [
	"nombre" => "Nacho" ,
	"apellido" => "Herrera",
	"edad" => "25",
	"notas" => array(3,4,2,4,6,7,9,10,3,7),
];
$alumno4 = [
	"nombre" => "Anxo" ,
	"apellido" => "Iglesias",
	"edad" => "32",
	"notas" => array(1,6,2,3.2,5,2.2,4.7,5.5,9,9),
];
$alumno5 = [
	"nombre" => "Valentina",
	"apellido" => "Iglesias",
	"edad" => "30",
	"notas" => array(9,7),
];
$alumnos = [
	$alumno1,
		$alumno2,
			$alumno3,
				$alumno4,
					$alumno5,

];
$cativos = array( );

$mediaedad = array();
$vellos = array();

$sumaC;
$totalC;
$mediaC;
$sumaM;
$totalM;
$mediaM;
$sumaV;
$totalV;
$mediaV;

echo "<div style='border: 1px solid black; width: 30%;'>";
foreach($alumnos as $alu){

			$suma = array_sum($alu["notas"]);
			$total = count($alu["notas"]);
			$media = $suma/$total;
			if($total >= 5 and $media > 5){

					echo "El alumno de nombre : " . $alu["nombre"] . " y apellido : ". $alu["apellido"]. " tiene una nota media de ". $media  . " APTO" . "<br>";
			}else{
					echo "El alumno de nombre : " . $alu["nombre"] . " y apellido : ". $alu["apellido"]. " tiene una nota media de ". $media    . "<br>";
			}



	if($alu["edad"]<29){
 array_push($cativos,$media);
 $sumaC = array_sum($cativos);
 $totalC = count($cativos);
 $mediaC = $sumaC/$totalC;
}
if($alu["edad"] > 29 and $alu["edad"] < 40){
	array_push($mediaedad,$media);
	$sumaM = array_sum($mediaedad);
	$totalM = count($mediaedad);
	$mediaM = $sumaM/$totalM;
	}
	if($alu["edad"] > 40 ){
		array_push($vellos,$media);
		$sumaV = array_sum($vellos);
		$totalV = count($vellos);
		$mediaV = $sumaV/$totalV;
		}

}



echo "La nota media de los alumnos con una edad entre [18 y 29] es de  "  . $mediaC . "<br>";
echo "La nota media de los alumnos con una edad entre [29 y 39] es de  "  . $mediaM . "<br>";
echo "La nota media de los alumnos con una edad de [40+] es de  "  . $mediaV . "<br>";




echo "<div style='border: 1px solid black; width: 30%;'>";





echo "</div>";


?>

</body>

<html>
